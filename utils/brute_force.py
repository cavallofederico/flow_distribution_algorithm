# -*- coding: utf-8 -*-

"""Brute force code for finding bi-objective pareto front 
of the combination of m x n options in the multi-knapsack problem. 
Each one of the m objects has to choose only one of the n sacks."""

from itertools import product
import numpy as np

def brute_force(m,n,A,B,T,L):
    """Function that returns the pareto front.
    m x n size matrix. m objects. n sacks. A: performance f1 of 
    combintation of object i with sack j. B: performance f2 of 
    combintation of object i with sack j. T: Maximum capacity of sacks.
    L: size of tasks"""
    f1 = []
    f2 = []
    combinations = []
    M = [n] * m
    objects = range(m)
    for a in multiradix_product(M):
        sacks_ocupation = [0] * n
        f1_ = 0
        f2_ = 0
        for (object_, sack) in zip(objects,a):
            sacks_ocupation[sack] += L[object_]
            f1_ += A[object_][sack]
            f2_ += B[object_][sack]
        if np.all(np.greater_equal(T,sacks_ocupation)):
            dominated = False
            for (f1_iter,f2_iter) in zip(f1,f2):
                if dominates((f1_iter,f2_iter),(f1_,f2_)):
                    dominated = True
            if not dominated:
                eliminate = []
                for (f1_iter,f2_iter,i) in zip(f1,f2,range(len(f1))):
                    if dominates((f1_,f2_),(f1_iter,f2_iter)):
                        eliminate.append(i)
                for index in sorted(eliminate, reverse=True):
                    del f1[index]
                    del f2[index]
                    del combinations[index]
                combinations.append(a)
                f1.append(f1_)
                f2.append(f2_)
    return combinations,f1,f2

def multiradix_product(M):
    return product(*(range(x) for x in M))
  
def dominates(a,b):
    """Returns True if b is dominated by a. False otherwise"""
    worse_than_other = a[0] <= b[0] and a[1] <= b[1]
    better_than_other = a[0] < b[0] or a[1] < b[1] 
    return worse_than_other and better_than_other

