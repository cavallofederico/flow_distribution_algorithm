# -*- coding: utf-8 -*-
"""
Minimum cost flow algorithms on directed connected graphs.
"""

__author__ = """Loïc Séguin-C. <loicseguin@gmail.com>"""
# Copyright (C) 2010 Loïc Séguin-C. <loicseguin@gmail.com>
# All rights reserved.
# BSD license.

# (2016)Modified by Federico Cavallo for BiObjective Optimization
# implementing the algorithm of Sedeno-Noda
# Sedeno-Noda - An algorithm for the biobjective integer minimum 
# cost flow problem
# 

__all__ = ['network_simplex']

from itertools import chain, islice, repeat
from math import ceil, sqrt
import networkx as nx
from networkx.utils import not_implemented_for
import numpy as np

try:
    from itertools import izip as zip
except ImportError:
    pass
try:
    range = xrange
except NameError:
    pass

    ###################################################################
    #Classes added for the biobjective problem
    ###################################################################

class Graph_Problem:
    """Container of the Graph problem to solve"""
    def __init__(self,n,edges,C,C2,u):
    
        self.edges = edges #list of the (p,q) edges
        self.n = n #amount of nodes
        self.C = C #matrix of the costs
        self.C2 = C2 #matrix of the costs
        self.u = u #matrix of the capacities

class Problem:
    """Class with the hole network problem
    C,C2 and u are vectors with the first cost, second cost and capacity of each arc"""
    def __init__(self,n,edges,C,C2,u,d):
        self.edges = edges
        self.d = d
        self.C = np.zeros((n,n))
        self.C2 = np.zeros((n,n))
        self.u = np.zeros((n,n))
        for (edge,c,c2,u_) in zip(edges,C,C2,u):
            self.C[edge[0],edge[1]] = c
            self.C2[edge[0],edge[1]] = c2
            self.u[edge[0],edge[1]] = u_
        self.points = []
        self.n = n #amount of nodes
        self.m = len(edges) #amount of edges

    def network_simplex(self,app_nodes,network_nodes,in_node,out_node):
        """Network simplex algorithm"""
        point = self.initial_tree2(app_nodes,network_nodes,in_node,out_node)
        self.compute_potentials(point)
        point.candidates_list = None
        point.last_node = 0
        e = self.entering_arc(point)
        i = 0
        while e != False:
            (l,delta,W),pivot_cycle = self.leaving_arc(e,point)
            self.update_tree(e,l,point) 
            self.update_potentials(l,e,point)
            #self.compute_potentials(point)
            self.update_x(delta,pivot_cycle,point)
            e = self.entering_arc(point)
            #if i == 10:
            #    return
            i += 1

        return

    def initial_tree(self):
        """Obtain the initial strong feaseble tree. (B,L,U), x, pi"""
        high_cost = 10 * np.max(self.C)
        high_capacity = np.sum(self.u)
        edges_ = list(self.edges)
        n = self.n
        d = self.d
        point = Point(n)
        point.pred.append(None)
        point.pred.extend([0]*(n-1))
        point.thread = list(range(1,n))
        point.thread.append(0)
        point.depth = [0]
        point.depth.extend([1]*(n-1))
        for j in range(1,self.n):
            if self.d[j] >= 0:
                if (0,j) not in edges_:
                    self.C[0][j] = high_cost
                    self.u[0][j] = high_capacity
                else:
                    edges_.remove((0,j))
                point.B.append((0,j))
                point.x[0][j] = d[j]
                
            else:
                if (j,0) not in edges_:
                    self.C[j][0] = high_cost
                    self.u[j][0] = high_capacity
                else:
                    edges_.remove((j,0))
                point.B.append((j,0))
                point.x[j][0] = -d[j]
        for e in edges_:
            point.L.append(e)
        self.points = [point]
        return point

    def initial_tree2(self,app_nodes,network_nodes,in_node,out_node):
        """Obtain the initial strong feaseble tree. (B,L,U), x, pi
        taking advantage of the problem caracteristics The root is
        the out node and all applications will be connected to the first
        network"""
        # 0 is the root and the out node
        # We consider the last node as the in node
        n = self.n
        point = Point(n)
        point.pred.append(None)
        point.pred.extend([0]*(n-1))
        point.thread = [0] * n
        point.depth = [0] * n
        first_network_node = network_nodes[0]
        second_network_node = network_nodes[1]
        first_app_node = app_nodes[0]
        second_app_node = app_nodes[1]
        last_app_node = app_nodes[-1]
        point.thread[out_node] = first_network_node
        point.thread[first_network_node] = first_app_node

        #point.thread[first_app_node] = in_node
        point.thread[first_app_node] = second_app_node

        #point.thread[in_node] = second_app_node
        l = len(network_nodes)
        point.x[first_network_node][out_node] = len(app_nodes)
        for index,node in enumerate(network_nodes):
            point.B.append((node,out_node))
            point.pred[node] = out_node #Every network parent is the out_node
            point.depth[node] = 1 
            if (index < l-1) and (node != first_network_node):
                point.thread[node] = network_nodes[index + 1]
            elif node != first_network_node:
                point.thread[node] = out_node
        l = len(app_nodes)
        for index,node in enumerate(app_nodes):
            point.pred[node] = first_network_node
            point.depth[node] = 2
            point.B.append((node,first_network_node))
            point.x[node][first_network_node] = 1
            #if node != first_app_node:
                #point.U.append((in_node,node))
                #point.x[in_node][node] = 1
            if (index < l - 1) and (node != first_app_node):
                point.thread[node] = app_nodes[index + 1]
            elif node != first_app_node:
                point.thread[node] = second_network_node
        #point.B.append((in_node,first_app_node))    #The in_node hangs from the first app node
        #point.x[in_node][first_app_node] = 1
        #point.pred[in_node] = first_app_node
        #point.depth[in_node] = 3
        for edge in self.edges:
            if edge not in point.B + point.U:
                point.L.append(edge)
        self.points = [point]
        return point

    def entering_arc(self,point,new_bucle=False):
        """Returns the entering arc with the candidate list rule"""
        n = self.n
        maximum = int(n/2)
        edges = point.L + point.U
        selected_edge = (None,0)
        if point.candidates_list:
            edges_to_remove = []
            for (edge,_) in point.cadidates_list:
                candidate_red_cost = self.reduced_cost(edge,point)
                if edge in point.L:
                    if candidate_red_cost >= 0:
                        edges_to_remove.append((edge,_))
                elif candidate_red_cost <= 0:
                    edges_to_remove.append((edge,_))
                elif selected_edge[1] < abs(candidate_red_cost):
                    selected_edge = (edge,abs(candidate_red_cost))
            for edge in edges_to_remove:
                point.candidates_list.remove(edge)
        
        if selected_edge[0]:
            point.candidates_list.remove(selected_edge)
            return selected_edge[0]
        candidates_list = []
        #Major bucle
        if new_bucle:
            i = 0
        else:
            i = point.last_node
        for j in range(n):
            for edge in [e for e in edges if e[0] == j]: #searching nodes en L or U emanating flow from node j
                candidate_red_cost = self.reduced_cost(edge,point)
                if edge in point.L:
                    if candidate_red_cost < 0:
                        candidates_list.append((edge,abs(candidate_red_cost)))
                        i += 1
                else:
                    if candidate_red_cost > 0:
                        candidates_list.append((edge,abs(candidate_red_cost)))
                        i += 1
                if i == maximum:
                    point.last_node = j
                    #print "break"
                    break
            point.last_node = j         #the next major bucle starts in the last node
        if not candidates_list and new_bucle:
            return False
        elif not candidates_list:
            edge = self.entering_arc(point,new_bucle=True)
            return edge
        #Minor bucle
        selected_edge = self.minor_iteration(candidates_list)
        return selected_edge[0]

    def update_tree(self,entering_arc,leaving_arc,point):
        """Updates B,L,U,pred,depth,thread"""
        """Function that updates the (B,L,U) of the tree after having reduced
        the flow in a pivot cycle"""

        if leaving_arc == entering_arc:
            if entering_arc in point.U:
                point.U.remove(entering_arc)
                point.L.append(entering_arc)
            else:
                point.L.remove(entering_arc)
                point.U.append(entering_arc)
            return
        #if leaving_arc in point.B:   #patch...
        point.B.remove(leaving_arc)
        if entering_arc in point.L:
            point.L.remove(entering_arc)
        else:
            point.U.remove(entering_arc)
        point.B.append(entering_arc)
        if point.x[leaving_arc[0]][leaving_arc[1]] == 0:
            point.L.append(leaving_arc)
        else:
            point.U.append(leaving_arc)
        #Determining f1,f2 and e1,e2
        if leaving_arc == entering_arc:
            return
        if point.depth[leaving_arc[0]] == point.depth[leaving_arc[1]] + 1:
            f_1 = leaving_arc[1] #first node leaving arc
            f_2 = leaving_arc[0] #second node leaving arc
        else:
            f_1 = leaving_arc[0] #first node leaving arc
            f_2 = leaving_arc[1] #second node leaving arc
        if entering_arc[0] != f_2:
            i = point.pred[entering_arc[0]]
            while i != f_2 and i != None:
                i = point.pred[i]
            if i != None:
                e_1 = entering_arc[1] #first node entering arc
                e_2 = entering_arc[0] #second node entering arc
            else:
                e_1 = entering_arc[0] #first node entering arc
                e_2 = entering_arc[1] #second node entering arc
        else:
                e_1 = entering_arc[1] #first node entering arc
                e_2 = entering_arc[0] #second node entering arc
        c = point.depth[e_1] - point.depth[e_2] + 1   #For updating depth
        new_depth = list(point.depth)
        l = e_2                                     #For setting predecesors
        #print "f1,f2", f_1,f_2
        #print "e1,e2", e_1,e_2
        #Updating the thread
        #Step 0: Initialize
        a = f_1
        while point.thread[a] != f_2:
            a = point.thread[a]
        b = point.thread[e_1]
        i = e_2
        k = i   
        #print "k:",k
        new_depth[k] = point.depth[k] + c #for updating depth
        #Step 1: Find last k and initialize r
        while point.depth[point.thread[k]] > point.depth[i]:
            k = point.thread[k]
            new_depth[k] = point.depth[k] + c        # for updating depth
            #print "k:", k
        #print "thread k:",point.thread[k]
        r = point.thread[k]
        j = None
        while True:
            #Step 2: If at the end of S*. remove S and insert S*.
            if i == f_2:
                point.thread[e_1] = e_2
                point.pred[e_2] = e_1
                #print e_1
                #print e_2
                #print "outside",j
                if j != None:
                    #print "inside",j
                    point.pred[f_2] = j
                if e_1 != a:
                    point.thread[a] = r
                    point.thread[k] = b
                    break
                else:
                    point.thread[k] = r
                    break
            #Step 3: Climb up the pivot stem and update s(A).
            j = i
            i = point.pred[i]
            point.pred[j] = l                        #Seting predecesors
            point.thread[k] = i
            #Step 4: Find the last node k in the left part of S,.
            k = i
            #print "k:",k
            c = c + 2
            new_depth[k] = point.depth[k] + c        # for updating depth
            while point.thread[k] != j:
                k = point.thread[k]
                #print "k:",k
                new_depth[k] = point.depth[k] + c    # for updating depth
            #print "thread k:",point.thread[k]
            #Step 5: [If the right part of S, is nonempty then 
            #update s(k), find the last node k in S,, and update r.
            if point.depth[r] > point.depth[i]:
                point.thread[k] = r
                while point.depth[point.thread[k]] > point.depth[i]:
                    k = point.thread[k]
                    #print "k:",k
                    new_depth[k] = point.depth[k] + c # for updating depth
                #print "thread k:",point.thread[k]
                r = point.thread[k]
            l = j  #Setting predecesors
        point.depth = new_depth

    def compute_potentials(self,point,k=0):
        """Function that computes pi_1 using C1"""
        root = 0
        if k == 0:
            C = self.C
        else:
            C = self.C2
        j = point.thread[root]
        point.pi[k][root] = 0.
        while j != root:
            i = point.pred[j]
            if (i,j) in point.B:
                point.pi[k][j] = point.pi[k][i] - C[i][j]
            if (j,i) in point.B:
                point.pi[k][j] = point.pi[k][i] + C[j][i]
            j = point.thread[j]

    def reduced_cost(self, edge,point,k=0):
        i = edge[0]
        j = edge[1]
        c_pi_ij = self.C[i][j] - point.pi[k][i] + point.pi[k][j]
        return c_pi_ij

    def minor_iteration(self,candidates_list):
        """For the entering arc selection"""
        selected_edge = (None,0)
        for (edge,candidate_red_cost) in candidates_list:
            if candidate_red_cost > selected_edge[1]:
                selected_edge = (edge,candidate_red_cost)
        candidates_list.remove(selected_edge)
        return selected_edge

    def leaving_arc(self, entering_arc,point):
        """Finds a cycle and returns delta, the maximum amount of 
        flow posible to increse"""

        i = entering_arc[0] #k
        j = entering_arc[1] #l
        depth = point.depth
        pred = point.pred
        u = self.u
        x = point.x
        delta_min = 0
        deltas = []

        if entering_arc in point.L:  #Determining the sense of the cycle W
            W = True
            deltas.append(((i,j),self.u[i][j], True)) #Entering arc
        else:
            W = False
            deltas.append(((i,j),point.x[i][j], False)) #Entering arc
        W_for = []      #W forward
        W_back = []     #W backward
        f = j
        t = i
        while i != j:
            if depth[i] > depth[j]:
                t = i
                i = pred[i]
                deltas.insert(0,self.delta(W,i,t,point))
            elif depth[j] > depth[i]:
                f = j
                j = pred[j]
                deltas.append(self.delta(W,f,j,point))

            else:
                t = i
                i = pred[i]
                f = j
                j = pred[j]
                deltas.insert(0,self.delta(W,i,t,point))
                deltas.append(self.delta(W,f,j,point))
        apex = i
        #print apex
        delta_min = min(deltas, key=lambda x: x[1])
        #Selection of the last blocking arc in the cycle starting by the apex
        if (deltas[0][0] == (i,f) or deltas[0][0] == (f,i)) and W==True:
            deltas.reverse()
        if (deltas[0][0] == (i,t) or deltas[0][0] == (t,i)) and W==False:
            deltas.reverse()

        for (edge,delta,_) in deltas:
            if delta == delta_min[1]:
                delta_min = (edge,delta,_)
        #print "Pivot cycle:", deltas
        return delta_min, deltas #deltas has the pivot cycle

    def update_x(self,delta,pivot_cycle,point):
        """Updates the flows in point.x"""
        for (edge,_,sense) in pivot_cycle:
            i = edge[0]
            j = edge[1]
            if sense == True:
                point.x[i][j] = point.x[i][j] + delta
            else:
                point.x[i][j] = point.x[i][j] - delta 

    def update_potentials(self,leaving_arc, entering_arc,point, t = 0):
        """Function that updates the potentials of the tree,
        implemented from Ahuja"""
        p = leaving_arc[0]
        q = leaving_arc[1]
        k = entering_arc[0]
        l = entering_arc[1]
        if point.depth[q] > point.depth[p]:
            y = q
        else:
            y = p
        i = k
        while i != l and i != None:
            i = point.pred[i]
        if i == None:       #k belongs to root Tree: T1
            change1 = -((self.C[k][l] - point.pi[t][k] + point.pi[t][l]))
        else:
            change1 = ((self.C[k][l] - point.pi[t][k] + point.pi[t][l]))
        point.pi[t][y] = point.pi[t][y] + change1
        z = point.thread[y]
        while point.depth[z] > point.depth[y]:
            point.pi[t][z] = point.pi[t][z] + change1
            z = point.thread[z]

    def delta(self,W,f,i,point):
        """Returns the delta in a cycle
        with sense W"""
        if W:
            if (f,i) in point.B:
                return ((f,i),self.u[f][i] - point.x[f][i],True)  # Means Forward arc
            else:
                return ((i,f),point.x[i][f], False)  # Means BackWards arc
        else:

            if (f,i) in point.B:
                return ((f,i),point.x[f][i],False)  # Means Backwards arc
            else:
                return ((i,f),self.u[i][f] - point.x[i][f],True)  # Means Forward arc

    def calculate_C2(self,ex=1):
        """Function that calculates the non-lineal C2 cost"""
        if ex == 0:
            return
        if ex == 1:
            B = [64,64,64]
            app_demand = [40,40,40]
            B_used = [0,0,0]
            for netw in range(3):
                for app in range(3):
                    B_used[netw] += self.points[0].x[app + 1,netw + 4]*app_demand[app]
                if B_used[netw] < 0.8 * B[netw]:
                    self.C2[netw+4][0] = 1
                    print 0
                elif B_used[netw] < B[netw]:
                    self.C2[netw+4][0] = 5
                    print 5
                else:
                    self.C2[netw+4][0] = 10
                    print 10
        elif ex == 2:
            app_demand = [500, 900, 1400] # 3 apps
            B = [2400, 1300, 1000]# 3 interfaces
            B_used = [0,0,0]
            for netw in range(3):
                for app in range(3):
                    B_used[netw] += self.points[0].x[app + 1,netw + 4]*app_demand[app]
                if B_used[netw] < B[netw]:
                    self.C2[netw+4][0] = 1
                    print 0
                else:
                    self.C2[netw+4][0] = 5
                #elif B_used[netw] <= B[netw]:
                #    self.C2[netw+4][0] = 8
                #    print 5
                #else:
                #    self.C2[netw+4][0] = 10
                #    print 10

        self.compute_potentials(self.points[0],k=1)
class Point:
    """Class to allocate un extreme point"""
    def __init__(self,n):
        self.B = []
        self.L = []
        self.U = []
        self.x = np.zeros((n,n))
        self.pi = [[0] * n, [0] * n] #normally pi[0],pi[1]
        self.thread = []
        self.depth = []
        self.pred = []

class Reduced_Efficient_Extreme_Point:
    """This class has only the important things for 
    the efficient ext point to work"""
    def __init__(self, p):
        self.B = list(p.B)
        self.U = list(p.U)
        self.L = list(p.L)
        self.x = p.new_edges_x.copy()
        self.pi_1 = list(p.pi_1)
        self.pi_2 = list(p.pi_2)

    def return_flow_cost(self,C,C2):
        """Returns the flow cost of each function, used for plotting"""
        cost_per_edge1 = np.multiply(self.x,C)
        cost_per_edge2 = np.multiply(self.x,C2)
        flow_cost1 = np.sum(cost_per_edge1)
        flow_cost2 = np.sum(cost_per_edge2)
        return flow_cost1,flow_cost2


class Efficient_Extreme_Point:
    """This class will allocate the extreme point"""
    def __init__(self):   
        pass
    def initialize(self, parent, size, next, prev, last, edge, x, pi, pi2, flow_cost, flow_cost2,f1_vector,f2_vector,F, T, u, C, C2):
        self.pred = parent
        self.size = size
        self.thread = next
        self.prev = prev
        self.last = last 
        self.edge = edge
        self.x = x
        self.pi_1 = pi
        self.pi_2 = pi2
        self.f1_vector = f1_vector
        self.f2_vector = f2_vector
        self.flow_cost = flow_cost
        self.flow_cost2 = flow_cost2
        self.F = F 
        self.T = T 
        self.u = u 
        self.C = C
        self.C2 = C2
        self.calculate_new_structure_edges()
        self.calculate_depth()
        self.create_B()

    def initialize_from_other_point(self,p):
        self.pred = list(p.pred)
        #self.size = list(p.size)
        self.thread = list(p.thread)
        #self.prev = list(p.prev)
        #self.last = list(p.last)
        #self.edge = list(p.edge)
        self.depth = list(p.depth)

        #self.x = list(p.x)
        self.pi_1 = list(p.pi_1)
        self.pi_2 = list(p.pi_2)
        self.f1_vector = list(p.f1_vector)
        self.f2_vector = list(p.f2_vector)
        self.flow_cost = p.flow_cost
        self.flow_cost2 = p.flow_cost2
        self.B = list(p.B) 
        self.L = list(p.L)
        self.U = list(p.U)  
        self.reduced_cost1 = list(p.reduced_cost1)
        self.reduced_cost2 = list(p.reduced_cost2)
        self.F = p.F 
        self.T = p.T 
        self.u = p.u 
        self.C = p.C
        self.C2 = p.C2
        self.new_edges_u = p.new_edges_u.copy()
        self.new_edges_x = p.new_edges_x.copy()
        self.new_edges_C = p.new_edges_C.copy()
        self.new_edges_C2 = p.new_edges_C2.copy()
        self.root = p.root
        if p.R:
            self.R = list(p.R)
        else:
            self.R = []
        if p.flow_cost1:
            self.flow_cost1 = p.flow_cost1
            self.flow_cost2 = p.flow_cost2

    def initialize_from_other_new_point(self,p,F,T,C,C2):
        self.pred = list(p.points[0].pred)
        self.thread = list(p.points[0].thread)
        self.depth = list(p.points[0].depth)
        self.pi_1 = list(p.points[0].pi[0])
        self.pi_2 = list(p.points[0].pi[1])
        self.B = list(p.points[0].B) 
        self.L = []
        self.U = []
        for edge in p.points[0].L:
            self.L.append(list(zip(F,T)).index(edge))
        for edge in p.points[0].U:
            self.U.append(list(zip(F,T)).index(edge))
        self.F = F 
        self.T = T 
        self.C = C
        self.C2 = C2
        self.new_edges_u = p.u.copy()
        self.new_edges_x = p.points[0].x.copy()
        self.new_edges_C = p.C.copy()
        self.new_edges_C2 = p.C2.copy()
        self.root = 0
        self.f1_vector = []
        self.f2_vector = []
        for edge in zip(F,T):
            self.f1_vector.append(p.points[0].x[edge[0]][edge[1]]*p.C[edge[0]][edge[1]])
            self.f2_vector.append(p.points[0].x[edge[0]][edge[1]]*p.C2[edge[0]][edge[1]])
        self.flow_cost = np.sum(np.multiply(p.points[0].x,p.C))
        self.flow_cost1 = np.sum(np.multiply(p.points[0].x,p.C))
        self.flow_cost2 = np.sum(np.multiply(p.points[0].x,p.C2))
        self.R = []
        self.u = p.u

    def define_non_tree_arcs(self):
        """Identify the non tree arcs for compatibility with NetworkX"""
        self.L = []
        self.U = []
        for (edge, source, target, x) in zip(range(len(self.F)), self.F, self.T, self.x):
            if edge != self.edge[source] and edge != self.edge[target]:
                if x == 0:
                    self.L.append(edge)
                else:
                    self.U.append(edge)

    def calculate_reduced_costs(self):
        """Function that calculates the reduced cost with the actual config"""
        self.reduced_cost1 = []
        self.reduced_cost2 = []
        for (source, target,c1,c2) in zip(self.F,self.T,self.C, self.C2):
            self.reduced_cost1.append(c1 - self.pi_1[source] + self.pi_1[target])
            self.reduced_cost2.append(c2 - self.pi_2[source] + self.pi_2[target])

    def calculate_depth(self):
        """Function to adapt the representation of the tree
        and set the depth parameter and separates the dummy root"""
        pred = self.pred
        thread = self.thread
        l = len(pred)

        self.depth = [None]*l #The depth parameter is created

        depth = self.depth
        l -= 1
        depth[l] = 0
        i = thread[l]

        depth[i] = 0
        i = thread[i]
        while i != -1:
            j = pred[i]
            if pred[i] != pred[j]:
                depth[i] = depth[j] + 1
            else:
                depth[i] = depth[j]
            i = thread[i]
          
        i = thread[l]       #desconecting the dummy root
        j = self.prev[l]
        pred[i] = pred[l]
        thread[j] = thread[l]
        self.root = thread[l] # creating root

    def calculate_new_structure_edges(self):
        """Function created for compatibility"""
        self.new_edges_u = np.zeros((len(self.thread)-1,len(self.thread)-1),dtype=int)
        self.new_edges_x = np.zeros((len(self.thread)-1,len(self.thread)-1),dtype=int)
        self.new_edges_C = np.zeros((len(self.thread)-1,len(self.thread)-1),dtype=float)
        self.new_edges_C2 = np.zeros((len(self.thread)-1,len(self.thread)-1),dtype=float)

        for (from_,target,capacity,x,c,c2) in zip(self.F,self.T,self.u,self.x,self.C,self.C2):
            self.new_edges_u[from_][target] = capacity
            self.new_edges_x[from_][target] = x
            self.new_edges_C[from_][target] = c
            self.new_edges_C2[from_][target] = c2

    def create_B(self): 
        """Function created for compatibility"""
        self.B = []
        #print "Edges:", self.edge
        #print "zip(F,T):", list(zip(self.F,self.T))
        for edge in self.edge:
            if edge != None and edge < len(self.F):
                #print "Edge:",edge
                self.B.append((self.F[edge],self.T[edge]))

    def find_cycle(self, entering_arc):
        """Finds a cycle and returns delta, the maximum amount of 
        flow posible to increse"""
        #print "Entering Arc(find_cycle): (", self.F[entering_arc], ",", self.T[entering_arc],")"
        #print "B:"
        #print self.B
        #print "L:"
        #for arc in self.L:
            #print self.arc_num_to_arc_nodes(arc)
        #print "U:"
        #for arc in self.U:  
            #print self.arc_num_to_arc_nodes(arc)
        i = self.F[entering_arc] #k
        j = self.T[entering_arc] #l
        depth = self.depth
        pred = self.pred
        u = self.new_edges_u
        x = self.new_edges_x
        delta_min = 0
        deltas = []

        if entering_arc in self.L:  #Determining the sense of the cycle W
            W = True
            deltas.append(((i,j),self.new_edges_u[i][j], True)) #Entering arc
        else:
            W = False
            deltas.append(((i,j),self.new_edges_x[i][j], False)) #Entering arc
        W_for = []      #W forward
        W_back = []     #W backward
#
        f = j
        t = i
#
        while i != j:
            if depth[i] > depth[j]:
                t = i
                i = pred[i]
                deltas.append(self.delta(W,i,t))
            elif depth[j] > depth[i]:
                f = j
                j = pred[j]
                deltas.append(self.delta(W,f,j))

            else:
                t = i
                i = pred[i]
                f = j
                j = pred[j]
                deltas.append(self.delta(W,i,t))
                deltas.append(self.delta(W,f,j))
        delta_min = min(deltas, key=lambda x: x[1])

#
        if (deltas[0][0] == (i,f) or deltas[0][0] == (f,i)) and W==True:
            deltas.reverse()
        if (deltas[0][0] == (i,t) or deltas[0][0] == (t,i)) and W==False:
            deltas.reverse()

        for (edge,delta,_) in deltas:
            if delta == delta_min[1]:
                delta_min = (edge,delta,_)
#
        #print "Pivot cycle:", deltas
        return delta_min, deltas #deltas has the pivot cycle

    def find_path(self, entering_arc, cost):
        """Finds a cycle and returns delta, the maximum amount of 
        flow posible to increse"""
        #print "Entering Arc(find_cycle): (", self.F[entering_arc], ",", self.T[entering_arc],")"
        #print "B:"
        #print self.B
        #print "L:"
        #for arc in self.L:
            #print self.arc_num_to_arc_nodes(arc)
        #print "U:"
        #for arc in self.U:  
            #print self.arc_num_to_arc_nodes(arc)
        i = self.F[entering_arc] #k
        j = self.T[entering_arc] #l
        depth = self.depth
        pred = self.pred
        u = self.new_edges_u
        x = self.new_edges_x
        delta_min = 0
        deltas = []

        if cost > 0:  #Determining the sense of the cycle W, if c>0 commes from lower bound
            W = True
            deltas.append(((i,j),self.new_edges_u[i][j], True)) #Entering arc
        else:
            W = False
            deltas.append(((i,j),self.new_edges_x[i][j], False)) #Entering arc
        W_for = []      #W forward
        W_back = []     #W backward
#
        f = j
        t = i
#
        while i != j:
            if depth[i] > depth[j]:
                t = i
                i = pred[i]
                deltas.append(self.delta(W,i,t))
            elif depth[j] > depth[i]:
                f = j
                j = pred[j]
                deltas.append(self.delta(W,f,j))

            else:
                t = i
                i = pred[i]
                f = j
                j = pred[j]
                deltas.append(self.delta(W,i,t))
                deltas.append(self.delta(W,f,j))
        delta_min = min(deltas, key=lambda x: x[1])

#
        if (deltas[0][0] == (i,f) or deltas[0][0] == (f,i)) and W==True:
            deltas.reverse()
        if (deltas[0][0] == (i,t) or deltas[0][0] == (t,i)) and W==False:
            deltas.reverse()

        for (edge,delta,_) in deltas:
            if delta == delta_min[1]:
                delta_min = (edge,delta,_)
#

        #print "Pivot cycle:", deltas
        return delta_min, deltas #deltas has the pivot cycle
        
    def delta(self,W,f,i):
        """Returns the delta in a cycle
        with sense W"""
        if W:
            if (f,i) in self.B:
                return ((f,i),self.new_edges_u[f][i] - self.new_edges_x[f][i],True)  # Means Forward arc
            else:
                return ((i,f),self.new_edges_x[i][f], False)  # Means BackWards arc
        else:

            if (f,i) in self.B:
                return ((f,i),self.new_edges_x[f][i],False)  # Means Backwards arc
            else:
                return ((i,f),self.new_edges_u[i][f] - self.new_edges_x[i][f],True)  # Means Forward arc

    def send_one_unit_flow(self,pivot_cycle):
        """Sends one unit flow throw the entering arc (p,q)"""
        for edge in pivot_cycle:
            if edge[2]: # if edge is Forward Arc
                self.new_edges_x[edge[0][0]][edge[0][1]] += 1
            else:
                self.new_edges_x[edge[0][0]][edge[0][1]] -= 1

    def augment_one_unit_flow(self,pivot_cycle):
        """Sends one unit flow throw the entering arc (p,q)"""
        for edge in pivot_cycle:
            if edge[2]: # if edge is Forward Arc
                self.new_edges_x[edge[0][0]][edge[0][1]] += 1
            else:
                self.new_edges_x[edge[0][0]][edge[0][1]] -= 1
    
    def update_BLU(self,leaving_arc, entering_arc):
        """Function that updates the (B,L,U) of the tree after having reduced
        the flow in a pivot cycle"""
        #print "Leaving arc:",self.arc_nodes_to_arc_nums(leaving_arc)
        #print "Leaving arc:", leaving_arc
        #print "Entering arc:",entering_arc
        #print "Entering arc:",self.arc_num_to_arc_nodes(entering_arc)
        #print "B:",self.B
        #print "U:",self.U
        #print "L:",self.L
        if leaving_arc == self.arc_num_to_arc_nodes(entering_arc):
            if entering_arc in self.U:
                self.U.remove(entering_arc)
                self.L.append(entering_arc)
            else:
                self.L.remove(entering_arc)
                self.U.append(entering_arc)
            return
        #if leaving_arc in self.B:   #patch...
        self.B.remove(leaving_arc)
        if entering_arc in self.L:
            self.L.remove(entering_arc)
        else:
            self.U.remove(entering_arc)
        self.B.append((self.F[entering_arc],self.T[entering_arc]))
        if self.new_edges_x[leaving_arc[0]][leaving_arc[1]] == 0:
            self.L.append(list(zip(self.F,self.T)).index(leaving_arc))
        else:
            self.U.append(list(zip(self.F,self.T)).index(leaving_arc))

        
        
    def update_dtp_pi(self,leaving_arc,entering_arc):
        """Function that updates the depth, thread and pred and potentials of the tree after having reduced
        the flow in a pivot cycle, from Chapter 9: Vasek Chvatal - Linear programming"""
        #Determining f1,f2 and e1,e2
        #self.update_potentials(leaving_arc,entering_arc)
        if leaving_arc == self.arc_num_to_arc_nodes(entering_arc):
            #print "entering_arc = leaving_arc"
            return
        if self.depth[leaving_arc[0]] == self.depth[leaving_arc[1]] + 1:
            f_1 = leaving_arc[1] #first node leaving arc
            f_2 = leaving_arc[0] #second node leaving arc
        else:
            f_1 = leaving_arc[0] #first node leaving arc
            f_2 = leaving_arc[1] #second node leaving arc
        if self.F[entering_arc] != f_2:
            i = self.pred[self.F[entering_arc]]
            while i != f_2 and i != None:
                i = self.pred[i]
            if i != None:
                e_1 = self.T[entering_arc] #first node entering arc
                e_2 = self.F[entering_arc] #second node entering arc
            else:
                e_1 = self.F[entering_arc] #first node entering arc
                e_2 = self.T[entering_arc] #second node entering arc
        else:
                e_1 = self.T[entering_arc] #first node entering arc
                e_2 = self.F[entering_arc] #second node entering arc
        c = self.depth[e_1] - self.depth[e_2] + 1   #For updating depth
        new_depth = list(self.depth)
        l = e_2                                     #For setting predecesors
        #print "f1,f2", f_1,f_2
        #print "e1,e2", e_1,e_2
        #Updating the thread
        #Step 0: Initialize
        a = f_1
        while self.thread[a] != f_2:
            a = self.thread[a]
        b = self.thread[e_1]
        i = e_2
        k = i   
        #print "k:",k
        new_depth[k] = self.depth[k] + c #for updating depth
        #Step 1: Find last k and initialize r
        while self.depth[self.thread[k]] > self.depth[i]:
            k = self.thread[k]
            new_depth[k] = self.depth[k] + c        # for updating depth
            #print "k:", k
        #print "thread k:",self.thread[k]
        r = self.thread[k]
        j = None
        while True:
            #Step 2: If at the end of S*. remove S and insert S*.
            if i == f_2:
                self.thread[e_1] = e_2
                self.pred[e_2] = e_1
                #print e_1
                #print e_2
                #print "outside",j
                if j != None:
                    #print "inside",j
                    self.pred[f_2] = j
                if e_1 != a:
                    self.thread[a] = r
                    self.thread[k] = b
                    break
                else:
                    self.thread[k] = r
                    break
            #Step 3: Climb up the pivot stem and update s(A).
            j = i
            i = self.pred[i]
            self.pred[j] = l                        #Seting predecesors
            self.thread[k] = i
            #Step 4: Find the last node k in the left part of S,.
            k = i
            #print "k:",k
            c = c + 2
            new_depth[k] = self.depth[k] + c        # for updating depth
            while self.thread[k] != j:
                k = self.thread[k]
                #print "k:",k
                new_depth[k] = self.depth[k] + c    # for updating depth
            #print "thread k:",self.thread[k]
            #Step 5: [If the right part of S, is nonempty then 
            #update s(k), find the last node k in S,, and update r.
            if self.depth[r] > self.depth[i]:
                self.thread[k] = r
                while self.depth[self.thread[k]] > self.depth[i]:
                    k = self.thread[k]
                    #print "k:",k
                    new_depth[k] = self.depth[k] + c # for updating depth
                #print "thread k:",self.thread[k]
                r = self.thread[k]
            l = j  #Setting predecesors
        self.depth = new_depth
        self.update_potentials(leaving_arc,entering_arc)

    def compute_potentials(self):
        """Function that computes pi_1 and pi_2 using C1 and C2"""
        root = self.root            
        j = self.thread[root]
        self.pi_1[root] = 0
        self.pi_2[root] = 0
        while j != root:
            i = self.pred[j]
            if (i,j) in self.B:
                self.pi_1[j] = self.pi_1[i] - self.new_edges_C[i][j]
                self.pi_2[j] = self.pi_2[i] - self.new_edges_C2[i][j]
            if (j,i) in self.B:
                self.pi_1[j] = self.pi_1[i] + self.new_edges_C[j][i]
                self.pi_2[j] = self.pi_2[i] + self.new_edges_C2[j][i]
            j = self.thread[j]

    def update_potentials(self,leaving_arc, entering_arc):
        """Function that updates the potentials of the tree,
        implemented from Ahuja"""
        p = leaving_arc[0]
        q = leaving_arc[1]
        k = self.F[entering_arc]
        l = self.T[entering_arc]
        if self.depth[q] > self.depth[p]:
            y = q
        else:
            y = p
        i = k
        while i != l and i != None:
            i = self.pred[i]
        if i == None:       #k belongs to root Tree: T1
            change1 = -((self.C[list(zip(self.F,self.T)).index((k,l))] - self.pi_1[k] + self.pi_1[l]))
            change2 = -((self.C2[list(zip(self.F,self.T)).index((k,l))] - self.pi_2[k] + self.pi_2[l]))
            #change1 = - self.reduced_cost1[entering_arc]
            #change2 = - self.reduced_cost2[entering_arc]
        else:
            change1 = ((self.C[list(zip(self.F,self.T)).index((k,l))] - self.pi_1[k] + self.pi_1[l]))
            change2 = ((self.C2[list(zip(self.F,self.T)).index((k,l))] - self.pi_2[k] + self.pi_2[l]))
            #change1 = self.reduced_cost1[entering_arc]
            #change2 = self.reduced_cost2[entering_arc]
        self.pi_1[y] = self.pi_1[y] + change1
        self.pi_2[y] = self.pi_2[y] + change2
        z = self.thread[y]
        while self.depth[z] > self.depth[y]:
            self.pi_1[z] = self.pi_1[z] + change1
            self.pi_2[z] = self.pi_2[z] + change2
            z = self.thread[z]

    def compute_flow_cost(self):
        """Function that computes the flow cost in the problem"""
        cost_per_edge1 = np.multiply(self.new_edges_x,self.new_edges_C)
        cost_per_edge2 = np.multiply(self.new_edges_x,self.new_edges_C2)
        self.flow_cost1 = np.sum(cost_per_edge1)
        self.flow_cost2 = np.sum(cost_per_edge2)
        #print "Cost per edge1:"
        #print cost_per_edge1
        #print "Cost per edge2:"
        #print cost_per_edge2
        #print "##############################################"
        #print "Flow Cost 1:",self.flow_cost1,"Flow Cost 2:", self.flow_cost2
        return self.flow_cost1, self.flow_cost2

    def arc_num_to_arc_nodes(self,arc):
        """Compatibility function"""
        return (self.F[arc],self.T[arc])
    def arc_nodes_to_arc_nums(self,arc):
        return list(zip(self.F,self.T)).index(arc)
        
    def return_flow_cost(self):
        """Returns the flow cost of each function, used for plotting"""
        cost_per_edge1 = np.multiply(self.new_edges_x,self.new_edges_C)
        cost_per_edge2 = np.multiply(self.new_edges_x,self.new_edges_C2)
        flow_cost1 = np.sum(cost_per_edge1)
        flow_cost2 = np.sum(cost_per_edge2)
        return flow_cost1,flow_cost2

    def calculate_C2(self,ex=1):
        """Function that calculates the non-lineal C2 cost"""
        if ex == 0:
            return
        if ex == 1:
            B = [64,64,64]
            app_demand = [40,40,40]
            B_used = [0,0,0]
            for netw in range(3):
                for app in range(3):
                    B_used[netw] += self.new_edges_x[app + 1,netw + 4]*app_demand[app]
                if B_used[netw] < 0.8 * B[netw]:
                    self.new_edges_C2[netw+4][0] = 1
                    print 0
                elif B_used[netw] < B[netw]:
                    self.new_edges_C2[netw+4][0] = 5
                    print 5
                else:
                    self.new_edges_C2[netw+4][0] = 10
                    print 10            
        elif ex == 2:
            #app_demand = [500, 900, 1400] # 3 apps
            #B = [2400, 1300, 1000]# 3 interfaces
            app_demand = [500, 900, 1400] # 3 apps
            B = [1600, 1000, 1400]# 3 interfaces
            B_used = [0,0,0]
            for netw in range(3):
                for app in range(3):
                    B_used[netw] += self.new_edges_x[app + 1,netw + 4]*app_demand[app]
                if B_used[netw] < B[netw]:
                    self.new_edges_C2[netw+4][0] = 1
                    print 0
                else:
                    self.new_edges_C2[netw+4][0] = 5
                #elif B_used[netw] <= B[netw]:
                #    self.new_edges_C2[netw+4][0] = 8
                #    print 5
                #else:
                #    self.new_edges_C2[netw+4][0] = 10
                #    print 10
        self.compute_potentials()

            
@not_implemented_for('undirected')
def network_simplex(G, demand='demand', capacity='capacity', weight='weight', weight2='weight2'):
    r"""Find a minimum cost flow satisfying all demands in digraph G.
    This is a primal network simplex algorithm that uses the leaving
    arc rule to prevent cycling.
    G is a digraph with edge costs and capacities and in which nodes
    have demand, i.e., they want to send or receive some amount of
    flow. A negative demand means that the node wants to send flow, a
    positive demand means that the node want to receive flow. A flow on
    the digraph G satisfies all demand if the net flow into each node
    is equal to the demand of that node.
    Parameters
    ----------
    G : NetworkX graph
        DiGraph on which a minimum cost flow satisfying all demands is
        to be found.
    demand : string
        Nodes of the graph G are expected to have an attribute demand
        that indicates how much flow a node wants to send (negative
        demand) or receive (positive demand). Note that the sum of the
        demands should be 0 otherwise the problem in not feasible. If
        this attribute is not present, a node is considered to have 0
        demand. Default value: 'demand'.
    capacity : string
        Edges of the graph G are expected to have an attribute capacity
        that indicates how much flow the edge can support. If this
        attribute is not present, the edge is considered to have
        infinite capacity. Default value: 'capacity'.
    weight : string
        Edges of the graph G are expected to have an attribute weight
        that indicates the cost incurred by sending one unit of flow on
        that edge. If not present, the weight is considered to be 0.
        Default value: 'weight'.
    weight2 : string
        Edges in this modificated version have an attribute weight
        that indicates a second cost incurred by sending one unit of
        flow on that edge. If not present, the weight is considered to 
        be 0.
    Returns
    -------
    flowCost : integer, float
        Cost of a minimum cost flow satisfying all demands.
    flowDict : dictionary
        Dictionary of dictionaries keyed by nodes such that
        flowDict[u][v] is the flow edge (u, v).
    Raises
    ------
    NetworkXError
        This exception is raised if the input graph is not directed,
        not connected or is a multigraph.
    NetworkXUnfeasible
        This exception is raised in the following situations:
            * The sum of the demands is not zero. Then, there is no
              flow satisfying all demands.
            * There is no flow satisfying all demand.
    NetworkXUnbounded
        This exception is raised if the digraph G has a cycle of
        negative cost and infinite capacity. Then, the cost of a flow
        satisfying all demands is unbounded below.
    Notes
    -----
    This algorithm is not guaranteed to work if edge weights or demands
    are floating point numbers (overflows and roundoff errors can
    cause problems).
    See also
    --------
    cost_of_flow, max_flow_min_cost, min_cost_flow, min_cost_flow_cost
    Examples
    --------
    A simple example of a min cost flow problem.
    >>> import networkx as nx
    >>> G = nx.DiGraph()
    >>> G.add_node('a', demand=-5)
    >>> G.add_node('d', demand=5)
    >>> G.add_edge('a', 'b', weight=3, capacity=4)
    >>> G.add_edge('a', 'c', weight=6, capacity=10)
    >>> G.add_edge('b', 'd', weight=1, capacity=9)
    >>> G.add_edge('c', 'd', weight=2, capacity=5)
    >>> flowCost, flowDict = nx.network_simplex(G)
    >>> flowCost
    24
    >>> flowDict # doctest: +SKIP
    {'a': {'c': 1, 'b': 4}, 'c': {'d': 1}, 'b': {'d': 4}, 'd': {}}
    The mincost flow algorithm can also be used to solve shortest path
    problems. To find the shortest path between two nodes u and v,
    give all edges an infinite capacity, give node u a demand of -1 and
    node v a demand a 1. Then run the network simplex. The value of a
    min cost flow will be the distance between u and v and edges
    carrying positive flow will indicate the path.
    >>> G=nx.DiGraph()
    >>> G.add_weighted_edges_from([('s', 'u' ,10), ('s' ,'x' ,5),
    ...                            ('u', 'v' ,1), ('u' ,'x' ,2),
    ...                            ('v', 'y' ,1), ('x' ,'u' ,3),
    ...                            ('x', 'v' ,5), ('x' ,'y' ,2),
    ...                            ('y', 's' ,7), ('y' ,'v' ,6)])
    >>> G.add_node('s', demand = -1)
    >>> G.add_node('v', demand = 1)
    >>> flowCost, flowDict = nx.network_simplex(G)
    >>> flowCost == nx.shortest_path_length(G, 's', 'v', weight='weight')
    True
    >>> sorted([(u, v) for u in flowDict for v in flowDict[u] if flowDict[u][v] > 0])
    [('s', 'x'), ('u', 'v'), ('x', 'u')]
    >>> nx.shortest_path(G, 's', 'v', weight = 'weight')
    ['s', 'x', 'u', 'v']
    It is possible to change the name of the attributes used for the
    algorithm.
    >>> G = nx.DiGraph()
    >>> G.add_node('p', spam=-4)
    >>> G.add_node('q', spam=2)
    >>> G.add_node('a', spam=-2)
    >>> G.add_node('d', spam=-1)
    >>> G.add_node('t', spam=2)
    >>> G.add_node('w', spam=3)
    >>> G.add_edge('p', 'q', cost=7, vacancies=5)
    >>> G.add_edge('p', 'a', cost=1, vacancies=4)
    >>> G.add_edge('q', 'd', cost=2, vacancies=3)
    >>> G.add_edge('t', 'q', cost=1, vacancies=2)
    >>> G.add_edge('a', 't', cost=2, vacancies=4)
    >>> G.add_edge('d', 'w', cost=3, vacancies=4)
    >>> G.add_edge('t', 'w', cost=4, vacancies=1)
    >>> flowCost, flowDict = nx.network_simplex(G, demand='spam',
    ...                                         capacity='vacancies',
    ...                                         weight='cost')
    >>> flowCost
    37
    >>> flowDict  # doctest: +SKIP
    {'a': {'t': 4}, 'd': {'w': 2}, 'q': {'d': 1}, 'p': {'q': 2, 'a': 2}, 't': {'q': 1, 'w': 1}, 'w': {}}
    References
    ----------
    .. [1] Z. Kiraly, P. Kovacs.
           Efficient implementation of minimum-cost flow algorithms.
           Acta Universitatis Sapientiae, Informatica 4(1):67--118. 2012.
    .. [2] R. Barr, F. Glover, D. Klingman.
           Enhancement of spanning tree labeling procedures for network
           optimization.
           INFOR 17(1):16--34. 1979.
    """

    ###########################################################################
    # Problem essentials extraction and sanity check
    ###########################################################################

    if len(G) == 0:
        raise nx.NetworkXError('graph has no nodes')

    # Number all nodes and edges and hereafter reference them using ONLY their
    # numbers

    N = list(G)                                # nodes
    I = {u: i for i, u in enumerate(N)}        # node indices
    D = [G.node[u].get(demand, 0) for u in N]  # node demands

    inf = float('inf')
    for p, b in zip(N, D):
        if abs(b) == inf:
            raise nx.NetworkXError('node %r has infinite demand' % (p,))

    multigraph = G.is_multigraph()
    S = []  # edge sources
    T = []  # edge targets
    if multigraph:
        K = []  # edge keys
    E = {}  # edge indices
    U = []  # edge capacities
    C = []  # edge weights
    C2 = [] # second edge weights

    if not multigraph:
        edges = G.edges(data=True)
    else:
        edges = G.edges(data=True, keys=True)
    edges = (e for e in edges
             if e[0] != e[1] and e[-1].get(capacity, inf) != 0)
    for i, e in enumerate(edges):
        S.append(I[e[0]])
        T.append(I[e[1]])
        if multigraph:
            K.append(e[2])
        E[e[:-1]] = i
        U.append(e[-1].get(capacity, inf))
        C.append(e[-1].get(weight, 0))
        C2.append(e[-1].get(weight2, 0))

    for e, c, c2 in zip(E, C, C2):
        if abs(c) == inf or abs(c2) == inf:
            raise nx.NetworkXError('edge %r has infinite weight' % (e,))
    if not multigraph:
        edges = G.selfloop_edges(data=True)
    else:
        edges = G.selfloop_edges(data=True, keys=True)
    for e in edges:
        if abs(e[-1].get(weight, 0)) == inf:
            raise nx.NetworkXError('edge %r has infinite weight' % (e[:-1],))
        if abs(e[-1].get(weight2, 0)) == inf:
            raise nx.NetworkXError('edge %r has infinite weight2' % (e[:-1],))

    ###########################################################################
    # Quick infeasibility detection
    ###########################################################################

    if sum(D) != 0:
        raise nx.NetworkXUnfeasible('total node demand is not zero')
    for e, u in zip(E, U):
        if u < 0:
            raise nx.NetworkXUnfeasible('edge %r has negative capacity' % (e,))
    if not multigraph:
        edges = G.selfloop_edges(data=True)
    else:
        edges = G.selfloop_edges(data=True, keys=True)
    for e in edges:
        if e[-1].get(capacity, inf) < 0:
            raise nx.NetworkXUnfeasible(
                'edge %r has negative capacity' % (e[:-1],))

    ###########################################################################
    # Initialization
    ###########################################################################

    # Add a dummy node -1 and connect all existing nodes to it with infinite-
    # capacity dummy edges. Node -1 will serve as the root of the
    # spanning tree of the network simplex method. The new edges will used to
    # trivially satisfy the node demands and create an initial strongly
    # feasible spanning tree.
    n = len(N)  # number of nodes
    for p, d in enumerate(D): # D is demand
        if d > 0:  # Must be greater-than here. Zero-demand nodes must have
                   # edges pointing towards the root to ensure strong
                   # feasibility.
            S.append(-1)
            T.append(p)
        else:
            S.append(p)
            T.append(-1)
    faux_inf = 3 * max(chain([sum(u for u in U if u < inf),
                              sum(abs(c) for c in C)],
                             (abs(d) for d in D))) or 1
    faux_inf2 = 3 * max(chain([sum(u for u in U if u < inf),
                              sum(abs(c2) for c2 in C2)],
                             (abs(d) for d in D))) or 1
    C.extend(repeat(faux_inf, n))
    C2.extend(repeat(faux_inf2, n))
    U.extend(repeat(faux_inf, n))

    # Construct the initial spanning tree.
    e = len(E)                                           # number of edges
    x = list(chain(repeat(0, e), (abs(d) for d in D)))   # edge flows
    pi = [faux_inf if d <= 0 else -faux_inf for d in D]  # node potentials
    pi2 = [faux_inf2 if d <= 0 else -faux_inf2 for d in D]  # second node potentials
    parent = list(chain(repeat(-1, n), [None]))  # parent nodes
    edge = list(range(e, e + n))                 # edges to parents
    size = list(chain(repeat(1, n), [n + 1]))    # subtree sizes
    next = list(chain(range(1, n), [-1, 0]))     # next nodes in depth-first thread
    prev = list(range(-1, n))                    # previous nodes in depth-first thread
    last = list(chain(range(n), [n - 1]))        # last descendants in depth-first thread

    ###########################################################################
    # Pivot loop
    ###########################################################################

    def reduced_cost(i):
        """Return the reduced cost of an edge i.
        """
        c = C[i] - pi[S[i]] + pi[T[i]]
        return c if x[i] == 0 else -c

    def reduced_cost2(i):
        """Return the reduced cost of an edge i.
        """
        c2 = C2[i] - pi2[S[i]] + pi2[T[i]]
        return c2 if x[i] == 0 else -c2

    def find_entering_edges():
        """Yield entering edges until none can be found.
        """
        if e == 0:
            return

        # Entering edges are found by combining Dantzig's rule and Bland's
        # rule. The edges are cyclically grouped into blocks of size B. Within
        # each block, Dantzig's rule is applied to find an entering edge. The
        # blocks to search is determined following Bland's rule.
        B = int(ceil(sqrt(e)))  # pivot block size
        M = (e + B - 1) // B    # number of blocks needed to cover all edges
        m = 0                   # number of consecutive blocks without eligible
                                # entering edges
        f = 0                   # first edge in block
        while m < M:
            # Determine the next block of edges.
            l = f + B
            if l <= e:
                edges = range(f, l)
            else:
                l -= e
                edges = chain(range(f, e), range(l))
            f = l
            # Find the first edge with the lowest reduced cost.
            i = min(edges, key=reduced_cost)
            c = reduced_cost(i)
            c2 = reduced_cost2(i)
            if c >= 0:
                # No entering edge found in the current block.
                m += 1
            else:
                # Entering edge found.
                if x[i] == 0:
                    p = S[i]
                    q = T[i]
                else:
                    p = T[i]
                    q = S[i]
                yield i, p, q
                m = 0
        # All edges have nonnegative reduced costs. The current flow is
        # optimal.

    def find_apex(p, q):
        """Find the lowest common ancestor of nodes p and q in the spanning
        tree.
        """
        size_p = size[p]
        size_q = size[q]
        while True:
            while size_p < size_q:
                p = parent[p]
                size_p = size[p]
            while size_p > size_q:
                q = parent[q]
                size_q = size[q]
            if size_p == size_q:
                if p != q:
                    p = parent[p]
                    size_p = size[p]
                    q = parent[q]
                    size_q = size[q]
                else:
                    return p

    def trace_path(p, w):
        """Return the nodes and edges on the path from node p to its ancestor
        w.
        """
        Wn = [p]
        We = []
        while p != w:
            We.append(edge[p])
            p = parent[p]
            Wn.append(p)
        return Wn, We

    def find_cycle(i, p, q):
        """Return the nodes and edges on the cycle containing edge i == (p, q)
        when the latter is added to the spanning tree.
        The cycle is oriented in the direction from p to q.
        """
        w = find_apex(p, q)
        Wn, We = trace_path(p, w)
        Wn.reverse()
        We.reverse()
        We.append(i)
        WnR, WeR = trace_path(q, w)
        del WnR[-1]
        Wn += WnR
        We += WeR
        return Wn, We


    def residual_capacity(i, p):
        """Return the residual capacity of an edge i in the direction away
        from its endpoint p.
        """
        return U[i] - x[i] if S[i] == p else x[i]

    def find_leaving_edge(Wn, We):
        """Return the leaving edge in a cycle represented by Wn and We.
        """
        j, s = min(zip(reversed(We), reversed(Wn)),
                   key=lambda i_p: residual_capacity(*i_p))
        t = T[j] if S[j] == s else S[j]
        return j, s, t

    def augment_flow(Wn, We, f):
        """Augment f units of flow along a cycle represented by Wn and We.
        """
        for i, p in zip(We, Wn):
            if S[i] == p:
                x[i] += f
            else:
                x[i] -= f

    def trace_subtree(p):
        """Yield the nodes in the subtree rooted at a node p.
        """
        yield p
        l = last[p]
        while p != l:
            p = next[p]
            yield p

    def remove_edge(s, t):
        """Remove an edge (s, t) where parent[t] == s from the spanning tree.
        """
        size_t = size[t]
        prev_t = prev[t]
        last_t = last[t]
        next_last_t = next[last_t]
        # Remove (s, t).
        parent[t] = None
        edge[t] = None
        # Remove the subtree rooted at t from the depth-first thread.
        next[prev_t] = next_last_t
        prev[next_last_t] = prev_t
        next[last_t] = t
        prev[t] = last_t
        # Update the subtree sizes and last descendants of the (old) acenstors
        # of t.
        while s is not None:
            size[s] -= size_t
            if last[s] == last_t:
                last[s] = prev_t
            s = parent[s]

    def make_root(q):
        """Make a node q the root of its containing subtree.
        """
        ancestors = []
        while q is not None:
            ancestors.append(q)
            q = parent[q]
        ancestors.reverse()
        for p, q in zip(ancestors, islice(ancestors, 1, None)):
            size_p = size[p]
            last_p = last[p]
            prev_q = prev[q]
            last_q = last[q]
            next_last_q = next[last_q]
            # Make p a child of q.
            parent[p] = q
            parent[q] = None
            edge[p] = edge[q]
            edge[q] = None
            size[p] = size_p - size[q]
            size[q] = size_p
            # Remove the subtree rooted at q from the depth-first thread.
            next[prev_q] = next_last_q
            prev[next_last_q] = prev_q
            next[last_q] = q
            prev[q] = last_q
            if last_p == last_q:
                last[p] = prev_q
                last_p = prev_q
            # Add the remaining parts of the subtree rooted at p as a subtree
            # of q in the depth-first thread.
            prev[p] = last_q
            next[last_q] = p
            next[last_p] = q
            prev[q] = last_p
            last[q] = last_p

    def add_edge(i, p, q):
        """Add an edge (p, q) to the spanning tree where q is the root of a
        subtree.
        """
        last_p = last[p]
        next_last_p = next[last_p]
        size_q = size[q]
        last_q = last[q]
        # Make q a child of p.
        parent[q] = p
        edge[q] = i
        # Insert the subtree rooted at q into the depth-first thread.
        next[last_p] = q
        prev[q] = last_p
        prev[next_last_p] = last_q
        next[last_q] = next_last_p
        # Update the subtree sizes and last descendants of the (new) ancestors
        # of q.
        while p is not None:
            size[p] += size_q
            if last[p] == last_p:
                last[p] = last_q
            p = parent[p]

    def update_potentials(i, p, q):
        """Update the potentials of the nodes in the subtree rooted at a node
        q connected to its parent p by an edge i.
        """
        if q == T[i]:
            d = pi[p] - C[i] - pi[q]
            d2 = pi2[p] - C2[i] - pi2[q]
        else:
            d = pi[p] + C[i] - pi[q]
            d2 = pi2[p] + C2[i] - pi2[q]
        for q in trace_subtree(q):
            pi[q] += d
            pi2[q] += d2


    # Pivot loop
    for i, p, q in find_entering_edges():
        Wn, We = find_cycle(i, p, q)
        j, s, t = find_leaving_edge(Wn, We)
        augment_flow(Wn, We, residual_capacity(j, s))
        if i != j:  # Do nothing more if the entering edge is the same as the
                    # the leaving edge.
            if parent[t] != s:
                # Ensure that s is the parent of t.
                s, t = t, s
            if We.index(i) > We.index(j):
                # Ensure that q is in the subtree rooted at t.
                p, q = q, p
            remove_edge(s, t)
            make_root(q)
            add_edge(i, p, q)
            update_potentials(i, p, q)

    ###########################################################################
    # Infeasibility and unboundedness detection
    ###########################################################################

    if any(x[i] != 0 for i in range(-n, 0)):
        raise nx.NetworkXUnfeasible('no flow satisfies all node demands')

    if (any(x[i] * 2 >= faux_inf for i in range(e)) or
        any(e[-1].get(capacity, inf) == inf and e[-1].get(weight, 0) < 0
            for e in G.selfloop_edges(data=True))):
        raise nx.NetworkXUnbounded(
            'negative cycle with infinite capacity found')

    ###########################################################################
    # Flow cost calculation and flow dict construction
    ###########################################################################

    del x[e:]
    flow_cost = sum(c * x for c, x in zip(C, x))
    flow_cost2 = sum(c2 * x for c2, x in zip(C2, x))
    f1_vector = [] #vector of flow cost
    for c_s, x_s in zip(C, x):
        f1_vector.append(c_s * x_s)
    f2_vector = [] # vector of flow cost2
    for c2_s, x_s in zip(C2, x):
        f2_vector.append(c2_s * x_s)
    flow_dict = {n: {} for n in N}

    def add_entry(e):
        """Add a flow dict entry.
        """
        d = flow_dict[e[0]]
        for k in e[1:-2]:
            try:
                d = d[k]
            except KeyError:
                t = {}
                d[k] = t
                d = t
        d[e[-2]] = e[-1]

    S_old = S #added for biobjective
    T_old = T

    S = (N[s] for s in S)  # Use original nodes.
    T = (N[t] for t in T)  # Use original nodes.
    if not multigraph:
        for e in zip(S, T, x):
            add_entry(e)
        edges = G.edges(data=True)
    else:
        for e in zip(S, T, K, x):
            add_entry(e)
        edges = G.edges(data=True, keys=True)
    for e in edges:
        if e[0] != e[1]:
            if e[-1].get(capacity, inf) == 0:
                add_entry(e[:-1] + (0,))
        else:
            c = e[-1].get(weight, 0)
            c2 = e[-1].get(weight2, 0)
            if c >= 0:
                add_entry(e[:-1] + (0,))
            else:
                u = e[-1][capacity]
                flow_cost += c * u
                flow_cost2 += c2 * u
                add_entry(e[:-1] + (u,))

    #edges = Edges_Matrix(x,f1_vector,f2_vector,G,C,C2)
    edge.append(None)
    #B = Tree(parent, size, next, prev, last, edge) 
    #extreme_1st = Efficient_Extreme_Point(B, x, pi, pi2, flow_cost, flow_cost2, flow_dict, edges)
    extreme_1st_new = Efficient_Extreme_Point()
    n2 = G.number_of_edges()
    extreme_1st_new.initialize(parent, size, next, prev, last, edge, x, pi, pi2, flow_cost, flow_cost2,f1_vector,f2_vector,S_old[0:n2], T_old[0:n2], U[0:n2], C[0:n2], C2[0:n2])

#    return extreme_1st
    return extreme_1st_new, S_old[0:n2], T_old[0:n2], U[0:n2], C[0:n2], C2[0:n2]

